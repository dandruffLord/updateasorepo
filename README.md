When called from each aso repo, pull latest branches from remote, as well as changes to the development and your current branch, and stand up the stack.

SETUP:
Clone this repo somewhere.
In "update.sh" in the cloned repo, change the variables in the EDITABLE VARIABLES section to reflect your aso repo setup.
You might have to change the access permissions of "update.sh" to allow it to execute, typically using the chmod command.
Add the cloned repo's path to your PATH variable (If you don't have admin access yet like me and can't change your global environment variables, add it to the PATH variable of your chosen terminal from its config file, in my case, .bashrc for Bash. If you did this, make sure to restart any running terminal processes so that the changes in your config file take effect).

USAGE:
Whenever you need to stand up the stack, just type "update.sh" in a terminal process in your working directory for each repo and press enter. This will search the directories listed in the PATH variable for "update.sh", and execute it when it finds it in the cloned repo. When "update.sh" executes, it will check the working directory to see if it matches the path of an aso repo in the EDITABLE VARIABLES section and if so, do its thing for the matched aso repo.

FLAGS:
-l
-lint
  If present, enables the linter when building for your registry aso repo.

NOTE:
Currently, you should only call "update.sh" in your oden aso repo after your registry aso repo is stood up (Has at least built its environment files).

TODO:
-Automatically determine aso repo instead of the user having to edit the EDITABLE VARIABLES section
-Merge upstream branch instead of remote branch with same name
-Implement a flag to run Storybook
-Overhaul in accordance with the registry aso repo integration of the library aso repo and partial composition of the design library aso repo
