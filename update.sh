#!/bin/sh
### EDITABLE VARIABLES ###
REPO_DIR_PATH='/c/Users/A0753565/Desktop/code/aso'
REGISTRY_REPO_PATH="$REPO_DIR_PATH"'/ui-component-registry'
LIBRARY_REPO_PATH="$REPO_DIR_PATH"'/ui-component-library'
ODEN_REPO_PATH="$REPO_DIR_PATH"'/oden'
DESIGN_REPO_PATH="$REPO_DIR_PATH"'/ui-design-library'

### HELPER FUNCTIONS ###
has_remote_branch_deps_changed() {
    BRANCH=$1

    git diff --quiet --exit-code origin/$BRANCH
    IS_REMOTE_CHANGED=$?
    if [ $IS_REMOTE_CHANGED == 0 ]
    then
        echo 0 && return 0
    fi
    git diff --quiet --exit-code origin/$BRANCH -- package.json
    IS_REMOTE_PACKAGE_CHANGED=$?
    if [ $IS_REMOTE_PACKAGE_CHANGED == 1 ]
    then
        echo 1 && return 0
    fi
    git diff --quiet --exit-code origin/$BRANCH -- package-lock.json
    IS_REMOTE_PACKAGE_LOCK_CHANGED=$?
    if [ $IS_REMOTE_PACKAGE_LOCK_CHANGED == 1 ]
    then
        echo 1 && return 0
    fi
    echo 0 && return 0
}

merge_remote_branch() {
    BRANCH=$1

    git merge origin/$BRANCH --no-edit
    IS_MERGE_CONFLICT=$?
    if [ $IS_MERGE_CONFLICT != 0 ]
    then
        exit 1
    fi
}

are_strings_equal() {
    STRING_ONE=$1
    STRING_TWO=$2

    [ "$STRING_ONE" = "$STRING_TWO" ]
    if [ $? == 0 ]
    then
        echo 1 && return 0
    fi
    echo 0 && return 0
}

is_linter_enabled() {
    FLAGS=$1

    echo "$FLAGS" | grep -E -i -w -q --m=1 "\-l(int)?"
    if [ $? == 0 ]
    then
        echo 1 && return 0
    fi
    echo 0 && return 0
}

### EXECUTING CODE ###
IS_LINTER_ENABLED=`is_linter_enabled $*`
DEV_BRANCH_NAME='development'
CURRENT_BRANCH_NAME=`git branch --show-current`
IS_CURRENT_BRANCH_DEV=`are_strings_equal "$DEV_BRANCH_NAME" "$CURRENT_BRANCH_NAME"`
git fetch origin
HAS_REMOTE_BRANCH_DEPS_CHANGED=`has_remote_branch_deps_changed "$CURRENT_BRANCH_NAME"`
if [ $HAS_REMOTE_BRANCH_DEPS_CHANGED != 1 -a $IS_CURRENT_BRANCH_DEV != 1 ]
then
    HAS_REMOTE_BRANCH_DEPS_CHANGED=`has_remote_branch_deps_changed "$DEV_BRANCH_NAME"`
fi
merge_remote_branch "$CURRENT_BRANCH_NAME"
if [ $IS_CURRENT_BRANCH_DEV != 1 ]
then
    merge_remote_branch "$DEV_BRANCH_NAME"
fi

case "$PWD" in
    "$REGISTRY_REPO_PATH")
        if [ $HAS_REMOTE_BRANCH_DEPS_CHANGED == 1 ]
        then
            npm run installLink
        fi
        if [ $IS_LINTER_ENABLED == 1 ]
        then
            npm run start:dev
        else
            npm run start:dev -- -x
        fi
    ;;
    "$LIBRARY_REPO_PATH")
        if [ $HAS_REMOTE_BRANCH_DEPS_CHANGED == 1 ]
        then
            npm install
        fi
        npm run build:dev
    ;;
    "$ODEN_REPO_PATH")
        if [ $HAS_REMOTE_BRANCH_DEPS_CHANGED == 1 ]
        then
            npm install
            npm link heapdump
        fi
        npm run start 2>&1 | tee logs/console.log
    ;;
    "$DESIGN_REPO_PATH")
        if [ $HAS_REMOTE_BRANCH_DEPS_CHANGED == 1 ]
        then
            npm install
        fi
        npm run watch:scss
    ;;
esac